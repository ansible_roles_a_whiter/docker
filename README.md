# docker



## Description

This role installing docker and docker-compose

## Include role

```
- src: https://gitlab.com/ansible_roles_a_whiter/docker.git
  version: "1.0"
  scm: git
  name: docker
```


## Example playbook

```
- hosts: docker
  gather_facts: true
  become: true

  roles:
    - {role: docker, when: ansible_system == "Linux"}

```

## Molecule test
It is highly recommended that you install molecule in a virtual environment.

#### Activate virtual environment
python3 -m venv venv
source venv/bin/activate

#### Installation Molecule

python3 -m pip install molecule ansible-core ansible-lint

python3 -m pip install "molecule-plugins[docker]"

